(function ($) {
  Drupal.behaviors.codeSampleSocialIntegrationAnalytics = {
    attach: function (context, settings) {

      var $dateFrom = $("#date-from"),
          $dateTo = $("#date-to");

      if ($dateFrom.length > 0 && $dateTo.length > 0) {
        $dateFrom.datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 2,
          dateFormat: "dd.mm.yy",
          maxDate: 0,
          onClose: function(selectedDate) {
            $dateTo.datepicker("option", "minDate", selectedDate);
          }
        });
        $dateTo.datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 2,
          dateFormat: "dd.mm.yy",
          maxDate: 0,
          onClose: function(selectedDate) {
            $dateFrom.datepicker("option", "maxDate", selectedDate);
          }
        });
      }
    }
  }
}(jQuery));
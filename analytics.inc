<?php

/**
 * Get statistics information.
 *
 * @param $provider
 *
 * @return mixed
 */
function code_sample_social_integration_get_statistics_by_date($provider) {
  return db_select('code_sample_social', 'lps')
    ->fields('lps', array('data'))
    ->condition('provider', $provider)
    ->orderBy('date', 'DESC')
    ->range(0, 1)
    ->execute()->fetchField();
}

/**
 * Analytics form callback.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function code_sample_social_integration_analytics_form($form, &$form_state) {
  $form['#attached'] = array(
    'js' => array(
      drupal_get_path('module', 'code_sample_twitter') . '/js/code_sample_twitter.js',
    ),
  );
  drupal_add_library('system', 'ui.datepicker');
  $default_date = date('d.m.Y');
  $default_type = 'day';

  $form['date_from'] = array(
    '#type' => 'textfield',
    '#title' => t('From'),
    '#attributes' => array(
      'id' => 'date-from',
    ),
    '#required' => TRUE,
    '#default_value' => $default_date,
  );
  $form['date_to'] = array(
    '#type' => 'textfield',
    '#title' => t('To'),
    '#attributes' => array(
      'id' => 'date-to',
    ),
    '#required' => TRUE,
    '#default_value' => $default_date,
  );
  $form['type'] = array(
    '#type' => 'select',
    '#options' => array(
      'day' => t('Day'),
      'week' => t('Week'),
      'month' => t('Month'),
      'year' => t('Year'),
    ),
    '#default_value' => $default_type,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
    '#name' => 'filter',
  );

  $provider_id = code_sample_social_integration_get_provider_tid();
  if (isset($form_state['clicked_button']) && $form_state['clicked_button']['#name'] == 'filter') {
    $analytics = code_sample_social_integration_render_analytics($form_state['values']['date_from'], $form_state['values']['date_to'], $provider_id, $form_state['values']['type']);
  }
  else {
    $analytics = code_sample_social_integration_render_analytics($default_date, $default_date, $provider_id, $default_type);
  }

  $form['analytics'] = array(
    '#type' => 'item',
    '#markup' => $analytics,
  );

  return $form;
}

/**
 * Analytics form submit.
 *
 * @param $form
 * @param $form_state
 */
function code_sample_social_integration_analytics_form_submit(&$form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

function code_sample_social_integration_render_analytics($start_date, $end_date, $provider_id, $type) {
  $items = array();
  // Get analytics information by chosen period.
  $statistics = db_select('code_sample_social', 'lps')
    ->fields('lps', array('date', 'data'))
    ->condition('lps.date', array(strtotime($start_date), strtotime("$end_date 23:59:59")), 'BETWEEN')
    ->condition('lps.provider', $provider_id)
    ->orderBy('date')
    ->execute()->fetchAllKeyed();

  // Get number of days by chosen type.
  switch($type) {
    case 'week':
      $days_count = 7;
      break;

    case 'month':
      $days_count = 30;
      break;

    case 'year':
      $days_count = 365;
      break;

    default:
      $days_count = 1;
      break;
  }

  $statistics = array_chunk($statistics, $days_count, TRUE);

  foreach($statistics as $key => $row) {
    // If in $row more than one element, so we need to compare only the first and the last items.
    if (count($row) > 1) {
      $timestamps = array_keys($row);

      // Get timestamps of the first and the last items.
      $first = $timestamps[0];
      $last = end($timestamps);

      // Get data of the first and the last items.
      $data_first = unserialize($row[$first]);
      $data_last = unserialize($row[$last]);

      // Get followers growth.
      $followers_growth = code_sample_social_integration_render_analytics_get_followers_growth($data_last, $data_first);

      $date = date('d.m.Y', $first) . '-' . date('d.m.Y', $last);
      $posts = $data_last['total_posts'];
      $_posts_growth = $data_last['total_posts'] - $data_first['total_posts'];
      $new_likes = $data_last['total_likes'] - $data_first['total_likes'];
      $likes_growth = $new_likes / $days_count;
      $likes_per_post = $data_last['total_likes'] / $posts;
      $comments_per_post = $data_last['total_comments'] / $posts; // ToDo: comments_total

      // Render item list element.
      $element = array(
        '#theme' => 'code_sample_social_integration_analytics',
        '#date' => date('d.m.Y', $first) . '-' . date('d.m.Y', $last),
        '#data' => array(
          'posts' => $posts,
          'new_likes' => $new_likes,
          'likes_growth' => $likes_growth,
          'likes_per_post' => $likes_per_post,
          'comments_per_post' => $comments_per_post,
        ),
      );
      $items[] = array($date, $posts, $new_likes, round($likes_growth*100,2).'%', round($likes_per_post,2), round($comments_per_post,2));
    }
    else {
      foreach($row as $date_key => $data) {
        $date = date('d.m.Y', $date_key);
        $data = unserialize($data);

        // Get prev day.
        if ($key > 0) {
          $prev = array_keys($statistics[$key - 1]);
          $prev_date = $prev[0];
          $prev_data = unserialize($statistics[$key - 1][$prev_date]);

          $likes_growth = $data['total_likes'] - $prev_data['total_likes'];
        }
        else {
          $likes_growth = '0%';
        }
        $posts = $data['total_posts'];
        $_posts_growth = $data['total_posts'];
        $new_likes = $data['total_likes'];

        $likes_per_post = $data['total_likes'] / $posts;
        $comments_per_post = $data['total_comments'] / $posts; // ToDo: comments_total

        // Render item list element.
        $element = array(
          '#theme' => 'code_sample_social_integration_analytics',
          '#date' => $date,
        );
      }
      $items[] = array($date, $posts, $new_likes, round($likes_growth*100,2).'%', round($likes_per_post,2), round($comments_per_post,2));
    }
  }

  $header = array(
    t('Date'), t('Posts'), t('New Likes'), t('Likes Growth %'), t('Likes per Post'), t('Comments per Post'),
  );
  return theme('table', array('header' => $header, 'rows' => $items));
}

/**
 * Get tweets count.
 *
 * @param $start_date
 * @param $end_date
 *
 * @return null|string
 */
function code_sample_social_integration_render_analytics_get_tweets_count($start_date, $end_date) {
  $statistics = db_select('field_data_field_post_date', 'fpd');
  $statistics->innerJoin('field_data_field_provider', 'fp', 'fp.entity_id = fpd.entity_id');
  $statistics->condition('fpd.field_post_date_value', array($start_date, $end_date), 'BETWEEN');
  $statistics->condition('fp.field_provider_tid', code_sample_social_integration_get_provider_tid());
  $statistics->addExpression('COUNT(*)');
  return $statistics->execute()->fetchField();
}

/**
 * Get followers_growth.
 *
 * @param $data
 * @param $prev_data
 *
 * @return string
 */
function code_sample_social_integration_render_analytics_get_followers_growth($data, $prev_data) {
  if (!isset($data['followers_count']) || !isset($prev_data['followers_count'])){
    return '0%';
  }

  if ($data['followers_count'] > $prev_data['followers_count']) {
    $symbol = '+';
  }
  elseif ($data['followers_count'] < $prev_data['followers_count']) {
    $symbol = '-';
  }
  else {
    return '0%';
  }

  return $symbol . round($data['followers_count'] * 100 / $prev_data['followers_count']) . '%';
}